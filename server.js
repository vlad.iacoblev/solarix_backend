const express = require("express");
const cookieSession = require("cookie-session");
const models = require("./app/models");
const config = require("./app/config");
const cron = require("node-cron");
const cors = require("cors");
const externalApi = require("./app/middlewares/externalApi");

const app = express();
app.use(express.json());

cron.schedule("1 * * * *", function () {
  externalApi.getMetadata();
});

app.use(express.urlencoded({ extended: true }));

app.use(
  cookieSession({
    name: "cryptosite",
    secret: config.jwtSecretKey,
    httpOnly: false,
  })
);

//used for logging de ex [Sun, 21 May 2023 19:34:14 GMT] ::1 - 304 Not Modified - GET /api/crypto/getAll HTTP/1.1 - undefined
app.use((req, res, next) => {
  res.on("finish", () => {
    console.log(
      `[${new Date().toUTCString()}] ${req.ip} - ${res.statusCode} ${
        res.statusMessage
      } - ${req.method} ${req.path} ${req.protocol.toUpperCase()}/${
        req.httpVersion
      } - ${req.username}`
    );
  });
  next();
});

app.use(
  cors({
    //origin: 'http://localhost:3000',
    origin: true,
    methods: "*",
    allowCredentials: true,
    credentials: true,
  })
);
//conexiunea la baza de date
const dbConnectionString = `mongodb://${config.dbUser}:${config.dbPass}@${config.dbHost}:${config.dbPort}/${config.dbName}?authSource=admin`;
models.db.mongoose
  .connect(dbConnectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log(`Database connected: ${dbConnectionString}`);
    initial();
  })
  .catch((err) => {
    console.error("Database connection error:", err);
    process.exit();
  });

require("./app/routes/auth.route")(app);
require("./app/routes/user.route")(app);
require("./app/routes/crypto.route")(app);
require("./app/routes/wallet.route")(app);
require("./app/routes/trades.route")(app);

app.listen(config.appPort, () => {
  console.log(`Node app started, listening on port ${config.appPort}`);
});

function initial() {
  //externalApi.getMetadata();
  externalApi.fetchCoinData();
  externalApi.getHistoricalData();
}
