const { account, user } = require("../models");
const isToday = require("dayjs/plugin/isToday");
const User = require("../models").user;
const dayjs = require("dayjs");
const isSameOrBefore = require("dayjs/plugin/isSameOrBefore");

dayjs().format();
dayjs.extend(isToday);
dayjs.extend(isSameOrBefore);

exports.publicAccess = (req, res) => {
  res.status(200).send("This is public content");
};

// exports.getRegisterStatistics = async (req, res) => {
//   try {
//     const users = await User.find({}, { password: 0, __v: 0 });
//     const todayUsers = users.filter(
//       (user) => !!dayjs(user.createdAt).isToday() === true
//     );
//     console.log(todayUsers);
//     res.status(200).send({ todayUsers: todayUsers.length });
//   } catch (err) {
//     console.log(err);
//     res.status(500).send({ message: "Internal Server Error" });
//   }
// };

exports.getRegisterStatistics = async (req, res) => {
  try {
    const users = await User.find({}, { password: 0, __v: 0 });

    const intervalStart = dayjs().subtract(6, "day");
    const intervalEnd = dayjs();

    const registerStatistics = [];

    for (
      let day = intervalStart;
      day.isSameOrBefore(intervalEnd);
      day = day.add(1, "day")
    ) {
      const registersOfDay = users.filter((user) =>
        dayjs(user.createdAt).isSame(day, "day")
      );
      registerStatistics.push({
        date: day.format("YYYY-MM-DD"),
        count: registersOfDay.length,
      });
    }

    res.status(200).send(registerStatistics);
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Internal Server Error" });
  }
};

exports.getUser = (req, res) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    res.status(200).send({
      id: user.id,
      username: user.username,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
    });
  });
};

exports.getAllUsers = async (req, res) => {
  try {
    if (!req.body) {
      res.status(400).send({ message: "Missing information" });
      return;
    }
    const users = await User.find({}, { password: 0, __v: 0 });
    res.status(200).send(users);
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Internal Server Error" });
  }
};

exports.adminAccess = (req, res) => {
  res.status(200).send("Some admin stuff");
};

exports.setProfile = async (req, res) => {
  if (!req.body) {
    res.status(400).send("Missing information.");
    return;
  }
  try {
    const user = await User.findById(req.userId).exec();
    if (req.body.firstName) user.firstName = req.body.firstName;
    if (req.body.lastName) user.lastName = req.body.lastName;
    if (req.body.email) user.email = req.body.email;
    user.save();
    res.status(200).send({ message: "Successfully saved profile" });
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Internal Server Error" });
    return;
  }
};

exports.setCard = async (req, res) => {
  if (
    !req.body ||
    !req.body.ccNumber ||
    !req.body.ccExpDate ||
    !req.body.ccCVV ||
    !req.body.ccName
  ) {
    res.status(400).send("Missing information.");
    return;
  }
  try {
    const user = await User.findById(req.userId).exec();
    console.log("User found:", user);
    user.number = req.body.ccNumber;
    user.expDate = req.body.ccExpDate;
    user.cvv = req.body.ccCVV;
    user.name = req.body.ccName;
    await user.save();
    console.log("User saved:", user);
    res.status(200).send({ message: "Successfully saved card" });
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Internal Server Error" });
    return;
  }
};
