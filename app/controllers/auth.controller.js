const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const config = require("../config");
const User = require("../models").user;
const Account = require("../models").account;

exports.register = async (req, res) => {
  try {
    const user = new User({
      username: req.body.username,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      isAdmin: false,
      number: req.body.ccNumber,
      expDate: req.body.ccExpDate,
      cvv: req.body.ccCVV,
      name: req.body.ccName,
      createdAt: req.body.createdAt,
    });
    await user.save();

    const account = new Account({
      username: req.body.username,
      BTC: 0,
      ETH: 0,
      XRP: 0,
      ADA: 0,
      SOL: 0,
      XLM: 0,
      AMP: 0,
      LRC: 0,
      DOGE: 0,
      EOS: 0,
      BUSD: 0,
      USDT: 0,
      USDC: 0,
      WalletUSD: 0,
    });
    await account.save();

    res.status(200).send({ message: "User registered" });
  } catch (error) {
    res.status(500).send({ message: "Internal Server Error" });
  }
};

exports.login = (req, res) => {
  if (!req.body.username || !req.body.password) {
    res.status(400).send({ message: "Missing information" });
    return;
  }

  User.findOne({ username: req.body.username }).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (!user) return res.status(404).send({ message: "User not found" });

    if (!bcrypt.compareSync(req.body.password, user.password))
      return res.status(401).send({ message: "Invalid credentials" });

    const token = jwt.sign(
      { id: user.id, username: user.username, email: user.email },
      config.jwtSecretKey,
      {
        expiresIn: 86400,
      }
    );
    req.session.token = token;
    res.status(200).send({
      message: "Logged in",
      id: user._id,
      username: user.username,
      email: user.email,
      isAdmin: user.isAdmin,
      token: token,
    });
  });
};

exports.logout = async (req, res) => {
  try {
    req.session = null;
    return res.status(200).send({ message: "Logged out" });
  } catch (err) {
    this.next(err);
  }
};
