const models = require("../models");
const config = require("../config");
const Trade = require("../models").trade;

exports.getUserTrades = async (req, res) => {
  try {
    const user = await models.user.findOne({ username: req.username }).exec();
    console.log(user);

    const trades = await models.trade
      .find({ username: req.username })
      .lean()
      .exec();

    res.status(200).send(trades);
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Internal Server Error" });
  }
};
