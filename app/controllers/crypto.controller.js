const models = require("../models");
const config = require("../config");
const Trade = require("../models").trade;

exports.getOneCrypto = async (req, res) => {
  if (!req.params || !req.params.symbol) {
    res.status(400).send({ message: "Missing information" });
    return;
  }
  console.log("the params is", req.params);
  const coinSymbols = config.trackedCoins.split(",");
  if (!coinSymbols.includes(req.params.symbol)) {
    res.status(400).send({ message: "Invalid symbol" });
    return;
  }
  try {
    const coin = await models.coin
      .findOne({ symbol: req.params.symbol })
      .lean()
      .exec();
    const coinData = await models.coinData
      .find({ symbol: req.params.symbol })
      .sort({ timestamp: -1 })
      .limit(1)
      .lean()
      .exec();
    const responseObject = { ...coin, ...coinData[0] };
    return res.status(200).send(responseObject);
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: "Internal Server Error" });
  }
};

exports.sendCryptoData = async (req, res) => {
  let responseObject = {};
  const coinSymbols = config.trackedCoins.split(",");
  for (const coinSymbol of coinSymbols) {
    try {
      const resultedCoinData = await models.coinData
        .find({ symbol: coinSymbol })
        .sort({ timestamp: -1 })
        .limit(1)
        .lean()
        .exec();
      const resultedCoin = await models.coin
        .find({ symbol: coinSymbol })
        .lean()
        .exec();
      responseObject[coinSymbol] = {
        ...resultedCoinData[0],
        ...resultedCoin[0],
      };
    } catch (err) {
      console.log(err);
      res.status(500).send({ message: "Internal Server Error" });
      return;
    }
  }
  responseObject.message = "Successfully fetched crypto data.";
  res.status(200).send(responseObject);
};

exports.sendCryptoHistory = async (req, res) => {
  if (!req.params || !req.params.symbol) {
    res.status(400).send("Missing information.");
    return;
  }
  const coinSymbols = config.trackedCoins.split(",");
  if (!coinSymbols.includes(req.params.symbol.toUpperCase())) {
    res.status(400).send({ message: "Invalid currency symbol" });
    return;
  }
  try {
    const resultedCoinData = await models.coinData
      .find({ symbol: req.params.symbol.toUpperCase() })
      .sort({ timestamp: 1 })
      .lean()
      .exec();
    res.status(200).send(resultedCoinData);
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Internal Server Error" });
    return;
  }
};

exports.tradeCrypto = async (req, res) => {
  if (
    !req.body ||
    !req.body.fromSymbol ||
    !req.body.fromAmount ||
    !req.body.toSymbol
  ) {
    res.status(400).send("Missing information.");
    return;
  }
  const coinSymbols = config.trackedCoins.split(",");
  if (
    !coinSymbols.includes(req.body.fromSymbol.toUpperCase()) ||
    !coinSymbols.includes(req.body.toSymbol.toUpperCase())
  ) {
    res.status(400).send({ message: "Invalid currency symbol" });
    return;
  }
  try {
    let account = await models.account
      .findOne({ username: req.username })
      .exec();
    const fromCrypto = req.body.fromSymbol.toUpperCase();
    const toCrypto = req.body.toSymbol.toUpperCase();
    const fromCryptoDatas = await models.coinData
      .find({ symbol: fromCrypto })
      .sort({ timestamp: -1 })
      .limit(100)
      .lean()
      .exec();
    const fromCryptoData = fromCryptoDatas[0];
    const toCryptoDatas = await models.coinData
      .find({ symbol: toCrypto })
      .sort({ timestamp: -1 })
      .limit(100)
      .lean()
      .exec();
    const toCryptoData = toCryptoDatas[0];
    account[fromCrypto] -= Number(req.body.fromAmount);
    if (account[fromCrypto] < 0) {
      res.status(403).send({ message: `You don't have enough ${fromCrypto}` });
      return;
    }
    const cryptoToGive =
      (Number(req.body.fromAmount) * fromCryptoData.priceInUSD) /
      toCryptoData.priceInUSD;
    account[toCrypto] += cryptoToGive;
    account.save();
    const transaction = new models.transaction({
      username: req.username,
      fromSymbol: req.body.fromSymbol,
      fromAmount: req.body.fromAmount,
      toSymbol: req.body.toSymbol,
      toAmount: cryptoToGive,
      fromFiat: false,
      toFiat: false,
      timestamp: new Date(),
    });
    transaction.save();
    res.status(200).send({
      message: `Traded ${req.body.fromAmount} ${req.body.fromSymbol} to ${cryptoToGive} ${req.body.toSymbol} successfully`,
    });
    return;
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Internal Server Error" });
    return;
  }
};

exports.buyCrypto = async (req, res) => {
  if (!req.body || !req.body.symbol || !req.body.amount) {
    res.status(400).send({ message: "Missing information." });
    return;
  }
  const coinSymbols = config.trackedCoins.split(",");
  if (!coinSymbols.includes(req.body.symbol.toUpperCase())) {
    res.status(400).send({ message: "Invalid currency symbol" });
    return;
  }
  try {
    const user = await models.user.findOne({ username: req.username }).exec();
    console.log(user);
    let account = await models.account
      .findOne({ username: req.username })
      .exec();
    console.log("The account is:", account);
    const crypto = req.body.symbol.toUpperCase();
    const cryptoDatas = await models.coinData
      .find({ symbol: crypto })
      .sort({ timestamp: -1 })
      .limit(100)
      .lean()
      .exec();
    const cryptoData = cryptoDatas[0];
    const price = cryptoData.priceInUSD;
    const finalPrice = price * Number(req.body.amount);
    console.log("finalPrice", finalPrice);
    console.log("wallteUSD", account.WalletUSD);
    if (account.WalletUSD < finalPrice) {
      res
        .status(403)
        .send({ message: "You don't have enough USD in your wallet." });
      return;
    }
    const trade = new Trade({
      username: user.username,
      fromSymbol: "USD",
      toSymbol: cryptoData.symbol,
      toAmount: req.body.amount,
      fromAmount: finalPrice,
      timestamp: new Date(),
    });
    await trade.save();

    // account["walletUSD"] -= price;
    account.WalletUSD -= finalPrice;
    account[crypto] += Number(req.body.amount);
    account.save();
    const transaction = new models.transaction({
      username: req.username,
      fromSymbol: "USD",
      fromAmount: price,
      toSymbol: crypto,
      toAmount: req.body.amount,
      fromFiat: true,
      toFiat: false,
      timestamp: new Date(),
    });
    transaction.save();
    res.status(200).send({
      message: `Bought ${req.body.amount} ${crypto} with ${price} Wallet USD successfully`,
    });
    return;
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Internal Server Error" });
    return;
  }
};

exports.sellCrypto = async (req, res) => {
  if (!req.body || !req.body.symbol || !req.body.amount) {
    res.status(400).send("Missing information.");
    return;
  }
  const coinSymbols = config.trackedCoins.split(",");
  if (!coinSymbols.includes(req.body.symbol.toUpperCase())) {
    res.status(400).send({ message: "Invalid currency symbol" });
    return;
  }
  try {
    const user = await models.user.findOne({ username: req.username }).exec();
    let account = await models.account
      .findOne({ username: req.username })
      .exec();
    const crypto = req.body.symbol.toUpperCase();
    const cryptoDatas = await models.coinData
      .find({ symbol: crypto })
      .sort({ timestamp: -1 })
      .limit(100)
      .lean()
      .exec();
    const cryptoData = cryptoDatas[0];

    const price = cryptoData.priceInUSD;
    const finalPrice = price * Number(req.body.amount);
    console.log("finalPrice", finalPrice);
    console.log("wallteUSD", account.WalletUSD);
    if (account[crypto] < Number(req.body.amount)) {
      res.status(403).send({ message: `You don't have enough ${crypto}` });
      return;
    }

    const trade = new Trade({
      username: user.username,
      fromSymbol: cryptoData.symbol,
      toSymbol: "USD",
      toAmount: finalPrice,
      fromAmount: req.body.amount,
      timestamp: new Date(),
    });
    await trade.save();
    // account["walletUSD"] += price;
    account.WalletUSD += finalPrice;
    account[crypto] -= Number(req.body.amount);
    account.save();

    const transaction = new models.transaction({
      username: req.username,
      fromSymbol: crypto,
      fromAmount: req.body.amount,
      toSymbol: "USD",
      toAmount: price,
      fromFiat: false,
      toFiat: true,
      timestamp: new Date(),
    });
    transaction.save();
    res.status(200).send({
      message: `Sold ${req.body.amount} ${crypto} with ${finalPrice} Wallet USD successfully`,
    });
    return;
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Internal Server Error" });
    return;
  }
};
