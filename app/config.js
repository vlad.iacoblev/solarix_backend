module.exports = {
  appPort: 8080,
  dbHost: "localhost",
  dbPort: 27017,
  dbName: "cryptosite",
  dbUser: "root",
  dbPass: "CrackerBarrel_900",
  jwtSecretKey: "Efn1xj8Q8T",
  externalApiEndpoint: "https://pro-api.coinmarketcap.com",
  externalApiKey: "ad2c7702-88ee-4b3f-8e58-567344d85a65",
  externalApiTestEndpoint: "https://sandbox-api.coinmarketcap.com",
  externalApiTestKey: "b54bcf4d-1bca-4e8e-9a24-22ff2c3d462c",
  historicalApiEndpoint: "https://coins.llama.fi",
  // historicalApiEndpoint: "https://rest.coinapi.io",
  // historicalApiKey: "D10DF6FA-9B8A-4526-83A9-010B47577991",
  // historicalApiKey: "94DE2423-C7BE-4F88-B5E5-29CDFAA5FC70",
  trackedCoins:
    "BTC,ETH,XRP,ADA,SOL,XLM,AMP,LRC,DOGE,EOS,BUSD,USDT,USDC,MATIC,XTZ",
  coigeckoIdMap: {
    BTC: "bitcoin",
    ETH: "ethereum",
    XRP: "ripple",
    ADA: "cardano",
    SOL: "solana",
    XLM: "stellar",
    AMP: "amp-token",
    LRC: "loopring",
    DOGE: "dogecoin",
    EOS: "eos",
    BUSD: "binance-usd",
    USDT: "tether",
    USDC: "usd-coin",
    XTZ: "tezos",
  },
};
