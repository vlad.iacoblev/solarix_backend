const jwt = require("jsonwebtoken");
const config = require("../config");

exports.verifyToken = (req, res, next) => {
  let token = req.session.token;

  if (!token) {
    return res.status(400).send({ message: "Missing authentication" });
  }

  jwt.verify(token, config.jwtSecretKey, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: "Unauthorized" });
    }
    req.userId = decoded.id;
    req.username = decoded.username;
    req.email = decoded.email;
    next();
  });
};
