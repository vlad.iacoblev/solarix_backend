const user = require("../models").user;

exports.isAdmin = (req, res, next) => {
    user.findById(req.userId).exec((err, user) => {
        if (err) {
            res.status(500).send({message: err});
            return;
        }
        if (!user.isAdmin) return res.status(403).send({message: "Unauthorized"});
        next();
    });
};
