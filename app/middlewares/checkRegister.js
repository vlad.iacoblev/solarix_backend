const User = require("../models").user;

module.exports = (req, res, next) => {
  if (
    !req.body.username ||
    !req.body.password ||
    !req.body.email ||
    !req.body.firstName ||
    !req.body.lastName
  ) {
    res.status(400).send({ message: "Missing information" });
    return;
  }

  User.findOne({ username: req.body.username }).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (user) {
      res.status(403).send({ message: "Username is taken" });
      return;
    }

    User.findOne({ email: req.body.email }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (user) {
        res.status(403).send({ message: "Email is already used" });
        return;
      }

      next();
    });
  });
};
