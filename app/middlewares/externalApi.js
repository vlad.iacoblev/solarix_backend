const axios = require("axios");
const config = require("../config");
const models = require("../models");

const updateCoin = (givenCoin) => {
  models.coin
    .findOne({ symbol: givenCoin.symbol })
    .exec((err, existingCoin) => {
      if (existingCoin) {
        existingCoin.symbol = givenCoin.symbol;
        existingCoin.id = givenCoin.id;
        existingCoin.name = givenCoin.name;
        existingCoin.logo = givenCoin.logo;
        existingCoin.description = givenCoin.description;
        existingCoin.save();
        console.log(`updated coin ${existingCoin.symbol}`);
      } else {
        new models.coin({
          symbol: givenCoin.symbol,
          id: givenCoin.id,
          name: givenCoin.name,
          logo: givenCoin.logo,
          description: givenCoin.description,
        }).save();
        console.log(`created coin ${givenCoin.symbol}`);
      }
    });
};

const addCoinData = (givenCoinData) => {
  new models.coinData({
    symbol: givenCoinData.symbol,
    id: givenCoinData.id,
    priceInUSD: givenCoinData.quote.USD.price,
    volume: givenCoinData.quote.USD.volume_24h,
    marketCap: givenCoinData.quote.USD.market_cap,
    circulatingSupply: givenCoinData.circulating_supply,
    totalSupply: givenCoinData.total_supply,
    maxSupply: givenCoinData.max_supply,
    timestamp: new Date(),
  }).save();
  console.log(
    `added new coin data: ${givenCoinData.symbol} = ${givenCoinData.quote.USD.price}`
  );
};

const addHistoricalCoinData = (givenCoinData) => {
  new models.coinData({
    symbol: givenCoinData.symbol,
    id: givenCoinData.id,
    priceInUSD: givenCoinData.priceInUSD,
    volume: null,
    marketCap: null,
    circulatingSupply: null,
    totalSupply: null,
    maxSupply: null,
    timestamp: new Date(givenCoinData.timestamp * 1000),
  }).save();
  console.log(
    `added new historical coin data: ${givenCoinData.symbol} = ${givenCoinData.priceInUSD} at ${givenCoinData.timestamp}`
  );
};

exports.fetchCoinData = () => {
  models.coinData
    .countDocuments({
      timestamp: { $gte: new Date(Date.now() - 30 * 60 * 1000) },
    })
    .exec((err, count) => {
      if (err) {
        console.log(err);
        return;
      }
      if (count > 0) {
        console.log("Coin data is already up to date. Skipping update.");
        return;
      }

      let response = null;
      new Promise(async (resolve, reject) => {
        try {
          response = await axios.get(
            `${config.externalApiEndpoint}/v2/cryptocurrency/quotes/latest?symbol=${config.trackedCoins}`,
            {
              headers: {
                "X-CMC_PRO_API_KEY": config.externalApiKey,
              },
            }
          );
        } catch (ex) {
          response = null;
          reject(ex);
        }
        if (response) {
          const json = response.data;
          resolve(json);
          Object.values(json.data).forEach((coin) => {
            addCoinData(coin[0]);
          });
        }
      });
    });
};

exports.getMetadata = () => {
  let response = null;
  new Promise(async (resolve, reject) => {
    try {
      response = await axios.get(
        `${config.externalApiEndpoint}/v2/cryptocurrency/info?symbol=${config.trackedCoins}`,
        {
          headers: {
            "X-CMC_PRO_API_KEY": config.externalApiKey,
          },
        }
      );
    } catch (ex) {
      response = null;
      reject(ex);
    }
    if (response) {
      const json = response.data;

      resolve(json);

      Object.values(json.data).forEach((coin) => {
        updateCoin(coin[0]);
      });
    }
  });
};

exports.getHistoricalData = () => {
  config.trackedCoins.split(",").forEach((coinSymbol) => {
    let coin = models.coin.findOne({ symbol: coinSymbol }).lean().exec();

    models.coinData
      .countDocuments({
        symbol: coinSymbol,
        timestamp: { $lte: new Date(1641117600 * 1000) },
      })
      .exec((err, count) => {
        if (err) {
          console.log(err);
          return;
        }
        if (count > 0) {
          console.log(
            `skipping historical data for ${coinSymbol} because it already exists`
          );
          return;
        }

        console.log("fetching historical data for " + coinSymbol);

        let response = null;
        new Promise(async (resolve, reject) => {
          try {
            response = await axios.get(
              `${config.historicalApiEndpoint}/chart/coingecko:${config.coigeckoIdMap[coinSymbol]}?start=1640988000&period=1d&span=467`
            );
          } catch (ex) {
            response = null;
            reject(ex);
          }
          if (response) {
            const json = response.data;
            resolve(json);
            const priceArray =
              json.coins["coingecko:" + config.coigeckoIdMap[coinSymbol]]
                .prices;
            priceArray.forEach((coinDataPoint) => {
              addHistoricalCoinData({
                symbol: coinSymbol,
                id: coin.id,
                priceInUSD: coinDataPoint.price,
                timestamp: coinDataPoint.timestamp,
              });
            });
          }
        });

        const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
        delay(250);
      });
  });
};
