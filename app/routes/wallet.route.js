const controller = require("../controllers/wallet.controller");
const jwt = require("../middlewares/jwt");

module.exports = (app) => {
  app.post("/api/wallet/deposit", [jwt.verifyToken], controller.deposit);
  app.post("/api/wallet/withdraw", [jwt.verifyToken], controller.withdraw);
  app.get("/api/wallet/getSum", [jwt.verifyToken], controller.getSum);
  app.get(
    "/api/wallet/getUserPortfolio",
    [jwt.verifyToken],
    controller.getUserPortfolio
  );
};
