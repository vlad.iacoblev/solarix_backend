const checkRegister = require("../middlewares/checkRegister");
const controller = require("../controllers/auth.controller");

module.exports = (app) => {
  app.post("/api/auth/register", [checkRegister], controller.register);
  app.post("/api/auth/login", controller.login);
  app.post("/api/auth/logout", controller.logout);
};
