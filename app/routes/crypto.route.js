const controller = require("../controllers/crypto.controller");
const jwt = require("../middlewares/jwt")

module.exports = (app) => {
    app.get("/api/crypto/getAll", controller.sendCryptoData);
    app.get("/api/crypto/getOne/:symbol", controller.getOneCrypto);
    app.get("/api/crypto/getHistory/:symbol", controller.sendCryptoHistory);
    app.post("/api/crypto/tradeCrypto", [jwt.verifyToken], controller.tradeCrypto);
    app.post("/api/crypto/buyCrypto", [jwt.verifyToken], controller.buyCrypto);
    app.post("/api/crypto/sellCrypto", [jwt.verifyToken], controller.sellCrypto);
};

