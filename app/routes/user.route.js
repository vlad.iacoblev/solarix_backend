const jwt = require("../middlewares/jwt");
const isAdmin = require("../middlewares/checkAdmin").isAdmin;
const userController = require("../controllers/user.controller");

module.exports = function (app) {
  app.get("/api/test/all", userController.publicAccess);
  app.get("/api/user/getUser", [jwt.verifyToken], userController.getUser);
  app.get(
    "/api/test/admin",
    [jwt.verifyToken, isAdmin],
    userController.adminAccess
  );
  app.post("/api/user/setCard", [jwt.verifyToken], userController.setCard);
  app.post(
    "/api/user/setProfile",
    [jwt.verifyToken],
    userController.setProfile
  );
  app.get(
    "/api/admin/getAllUsers",
    [jwt.verifyToken, isAdmin],
    userController.getAllUsers
  );
  app.get(
    "/api/admin/getRegisterStatistics/",
    [jwt.verifyToken],
    userController.getRegisterStatistics
  );
};
