const controller = require("../controllers/trade.controller");
const jwt = require("../middlewares/jwt");

module.exports = (app) => {
  app.get(
    "/api/trades/UserTrades",
    [jwt.verifyToken],
    controller.getUserTrades
  );
};
