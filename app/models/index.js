const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = { mongoose: mongoose };

const User = mongoose.model(
  "User",
  new mongoose.Schema({
    username: String,
    email: String,
    password: String,
    firstName: String,
    lastName: String,
    number: String,
    expDate: String,
    cvv: String,
    name: String,
    isAdmin: Boolean,
    createdAt: Date,
  })
);
db.user = User;

const Coin = mongoose.model(
  "Coin",
  new mongoose.Schema({
    symbol: String,
    id: Number,
    name: String,
    description: String,
    logo: String,
  })
);
db.coin = Coin;

const CoinData = mongoose.model(
  "CoinData",
  new mongoose.Schema({
    symbol: String,
    id: Number,
    priceInUSD: Number,
    volume: Number,
    marketCap: Number,
    circulatingSupply: Number,
    totalSupply: Number,
    maxSupply: Number,
    timestamp: Date,
  })
);
db.coinData = CoinData;

const Account = mongoose.model(
  "Account",
  new mongoose.Schema({
    username: String,
    BTC: Number,
    ETH: Number,
    XRP: Number,
    ADA: Number,
    SOL: Number,
    XLM: Number,
    AMP: Number,
    LRC: Number,
    DOGE: Number,
    EOS: Number,
    BUSD: Number,
    USDT: Number,
    USDC: Number,
    WalletUSD: Number,
  })
);
db.coinData = Account;

const Transaction = mongoose.model(
  "Transaction",
  new mongoose.Schema({
    username: String,
    fromSymbol: String,
    fromAmount: Number,
    toSymbol: String,
    toAmount: Number,
    fromFiat: Boolean,
    toFiat: Boolean,
    timestamp: Date,
  })
);
db.coinData = Transaction;

const Trade = mongoose.model(
  "Trade",
  new mongoose.Schema({
    username: String,
    fromSymbol: String,
    toSymbol: String,
    fromAmount: Number,
    toAmount: Number,
    timestamp: Date,
  })
);
db.coinData = Trade;

module.exports.user = User;
module.exports.coin = Coin;
module.exports.coinData = CoinData;
module.exports.account = Account;
module.exports.transaction = Transaction;
module.exports.trade = Trade;
module.exports.db = db;
