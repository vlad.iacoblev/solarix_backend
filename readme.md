# Crypto backend

solarix

## Database

Database uses MongoDB as a Docker container, defined in Docker Compose. Also included is a web interface for Mongo called Mongo-Express. Image tags are latest.

To start, run `docker compose up -d`.

## Backend app

The backend node application uses nodemon for development.

To start, run `npm start`.

## User system
The user system uses JWT tokens from the cookie storage. Upon logging in, cookies containing a JWT token are set.
When requesting data that requires authorization, the cookies are checked for the JWT token.

Note: since cookies are used, no authorization header or JSON data needs to be sent.

## API endpoints

Note: all parameters must be encoded in the request body using JSON

List of all endpoints:

| Endpoint                    | Description               |
|-----------------------------|---------------------------|
| /api/auth/register          | Register a new user       |
| /api/auth/login             | Log in a user             |
| /api/auth/logout            | Log out a user            |
| /api/user/getUser           | Get user data             |
| /api/user/setProfile        | Set user profile data     |
| /api/user/setCard           | Set user credit card data |
| /api/crypto/getAll          | Get all crypto            |
| /api/crypto/getOne/{symbol} | Get one crypto            |
| /api/crypto/getHistory      | Get crypto history data   |
| /api/crptyo/sellCrypto      | Sell crypto               |
| /api/crptyo/buyCrypto       | Buy crypto                |
| /api/crypto/tradeCrypto     | Trade crypto              |
| /api/wallet/getSum          | Get wallet sum            |
| /api/wallet/withdraw        | Withdraw from wallet      |
| /api/wallet/deposit         | Deposit to wallet         |

---

### `POST /api/auth/register`
| Parameter | Type   | Required? |
|-----------|--------|-----------|
| username  | String | Yes       |
| password  | String | Yes       |
| email     | String | Yes       |
| firstName | String | Yes       |
| lastName  | String | Yes       |

Possible responses:

| Message               | Status          | Additional JSON Data |
|-----------------------|-----------------|----------------------|
| User registered       | 200 OK          |                      |
| Missing information   | 400 Bad Request |                      |
| Username is taken     | 403 Forbidden   |                      |
| Email is already used | 403 Forbidden   |                      |
---
### `POST /api/auth/login`
| Parameter | Type   | Required? |
|-----------|--------|-----------|
| username  | String | Yes       |
| password  | String | Yes       |

Possible responses:

| Message             | Status           | Additional JSON Data                    |
|---------------------|------------------|-----------------------------------------|
| Logged in           | 200 OK           | { message, id, username, email, token } |
| Missing information | 400 Bad Request  |                                         |
| Invalid credentials | 401 Unauthorized |                                         |
| User not found      | 404 Not Found    |                                         |
---
### `POST /api/auth/logout`
No parameters.

Possible responses:

| Message             | Status           | Additional JSON Data |
|---------------------|------------------|----------------------|
| Logged out          | 200 OK           |                      |
---

### `GET /api/user/getUser`
No parameters. Must be logged in.

Possible responses:

| Message | Status           | Additional JSON Data                         |
|---------|------------------|----------------------------------------------|
|         | 200 OK           | { id, username, email, firstName, lastName } |
---
### `POST /api/user/setProfile`
Must be logged in.

Parameters:

| Parameter | Type   | Required? |
|-----------|--------|-----------|
| firstName | String | No        |
| lastName  | String | No        |
| email     | String | No        |
Note: At least one parameter required. Does not accept empty request.

Possible responses:

| Message                    | Status           | Additional JSON Data |
|----------------------------|------------------|----------------------|
| Successfully saved profile | 200 OK           |                      |
---
### `POST /api/user/setCard`
Must be logged in.

Parameters:

| Parameter | Type   | Required? |
|-----------|--------|-----------|
| ccNumber  | String | Yes       |
| ccExpDate | String | Yes       |
| ccCVV     | String | Yes       |
| ccName    | String | Yes       |

Possible responses:

| Message                 | Status          | Additional JSON Data |
|-------------------------|-----------------|----------------------|
| Successfully saved card | 200 OK          |                      |
| Missing information     | 400 Bad Request |                      |
---
## Crypto data system

### `GET /api/crypto/getAll`
No parameters.

Possible responses:

| Message                          | Status           | Additional JSON Data    |
|----------------------------------|------------------|-------------------------|
| Successfully fetched crypto data | 200 OK           | Crypto data, see below. |
Crypto data JSON example:
```json
{
    "Cypto1": {
        "_id": "123456789asdf",
        "symbol": "CR1",
        "id": 1,
        "priceInUSD": 123.456,
        "volume": 998877.6654,
        "marketCap": 56473.222,
        "circulatingSupply": 193276,
        "totalSupply": 1932762,
        "maxSupply": 2932762,
        "timestamp": "2023-03-25T18:00:00.100Z",
        "__v": 0,
        "name": "Cryto 1",
        "logo": "https://www.example.com/img/crypto1_logo.png"
    },
    "Crypto2": {
		...
    },
    ...
}
```
---
### `GET /api/crypto/getHistory`
| Parameter | Type   | Required? |
|-----------|--------|-----------|
| symbol    | String | Yes       |

Possible responses:

| Message                             | Status           | Additional JSON Data       |
|-------------------------------------|------------------|----------------------------|
| Successfully fetched crypto history | 200 OK           | Crypto history, see below. |
Crypto history JSON example:
```json
[
  {
    "_id": "123456789asdf",
    "symbol": "CR1",
    "id": 1,
    "priceInUSD": 123.456,
    "volume": 998877.6654,
    "marketCap": 56473.222,
    "circulatingSupply": 193276,
    "totalSupply": 1932762,
    "maxSupply": 2932762,
    "timestamp": "2023-03-25T18:00:00.100Z",
    "__v": 0
  },
  {
    "_id": "123456789asdf",
    "symbol": "CR1",
    "id": 1,
    "priceInUSD": 223.456,
    "volume": 998874.6654,
    "marketCap": 56483.222,
    "circulatingSupply": 193274,
    "totalSupply": 1932760,
    "maxSupply": 2932762,
    "timestamp": "2023-03-25T17:00:00.100Z",
    "__v": 0
  },
  {
    ...
  },
  ...
]
```
---
### `POST /api/crypto/tradeCrypto`
Must be logged in.

Parameters:

| Parameter  | Type   | Required? |
|------------|--------|-----------|
| fromSymbol | String | Yes       |
| fromAmount | Number | Yes       |
| toSymbol   | String | Yes       |

Possible responses:

| Message                                  | Status           | Additional JSON Data |
|------------------------------------------|------------------|----------------------|
| Traded X CRYPTO to Y CRYPTO successfully | 200 OK           |                      |
| Missing information                      | 400 Bad Request  |                      |
| You don't have enough CRYPTO             | 403 Forbidden    |                      |
| Unauthorized                             | 401 Unauthorized |                      |
| Invalid currency symbol                  | 400 Bad Request  |                      |

---
### `POST /api/crypto/buyCrypto`
Must be logged in.

Parameters:

| Parameter | Type   | Required? |
|-----------|--------|-----------|
| symbol    | String | Yes       |
| amount    | Number | Yes       |

Possible responses:

| Message                                        | Status           | Additional JSON Data |
|------------------------------------------------|------------------|----------------------|
| Bought X CRYPTO with Y Wallet USD successfully | 200 OK           |                      |
| Missing information                            | 400 Bad Request  |                      |
| You don't have enough Wallet USD               | 403 Forbidden    |                      |
| Unauthorized                                   | 401 Unauthorized |                      |
| Invalid currency symbol                        | 400 Bad Request  |                      |

---
### `POST /api/crypto/sellCrypto`
Must be logged in.

Parameters:

| Parameter | Type   | Required? |
|-----------|--------|-----------|
| symbol    | String | Yes       |
| amount    | Number | Yes       |

Possible responses:

| Message                                      | Status            | Additional JSON Data |
|----------------------------------------------|-------------------|----------------------|
| Sold X CRYPTO with Y Wallet USD successfully | 200 OK            |                      |
| Missing information                          | 400 Bad Request   |                      |
| You don't have enough CRYPTO                 | 403 Forbidden     |                      |
| Unauthorized                                 | 401 Unauthorized  |                      |
| Invalid currency symbol                      | 400 Bad Request   |                      |

---
### `GET /api/wallet/getSum`
No parameters. Must be logged in.

Possible responses:

| Message      | Status           | Additional JSON Data |
|--------------|------------------|----------------------|
|              | 200 OK           | { WalletUSD }        |
| Unauthorized | 401 Unauthorized |                      |

---
### `POST /api/wallet/deposit`
Must be logged in.

Parameters:

| Parameter | Type   | Required? |
|-----------|--------|-----------|
| amount    | Number | Yes       |

Possible responses:

| Message                              | Status           | Additional JSON Data |
|--------------------------------------|------------------|----------------------|
| Succesfully deposited X Wallet USD   | 200 OK           |                      |
| Missing information                  | 400 Bad Request  |                      |
| Missing credit card. Please add one. | 403 Forbidden    |                      |
| Unauthorized                         | 401 Unauthorized |                      |

---
### `POST /api/wallet/withdraw`
Must be logged in.

Parameters:

| Parameter | Type   | Required? |
|-----------|--------|-----------|
| amount    | Number | Yes       |

Possible responses:

| Message                              | Status            | Additional JSON Data |
|--------------------------------------|-------------------|----------------------|
| Succesfully withdrew X Wallet USD    | 200 OK            |                      |
| Missing information                  | 400 Bad Request   |                      |
| You don't have enough Wallet USD     | 403 Forbidden     |                      |
| Missing credit card. Please add one. | 403 Forbidden     |                      |
| Unauthorized                         | 401 Unauthorized  |                      |

